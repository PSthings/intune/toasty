$path="C:\ProgramData\toasty\Welcome-ToastNotification.ps1"
# Check profile folder creation date. Only show popup if user profile is less than $cutofftime old
$Usercreated = ((Get-Date) - (Get-Childitem C:\Users\($env:username)).CreationTime).Days
# days time limit until user will get the shortcut deployed
$cutofftime = "1"
If ($Usercreated -lt $cutofftime) {
Invoke-WebRequest https://path/to/badgeimage.png -OutFile ( New-Item -Path "C:\ProgramData\toasty\badgeimage.png" -Force )
Invoke-WebRequest https://path/to/heroimage.jpg -OutFile ( New-Item -Path "C:\ProgramData\toasty\heroimage.jpg" -Force )
Invoke-WebRequest https://path/to/CustomMessage.xml -OutFile ( New-Item -Path "C:\ProgramData\toasty\CustomMessage.xml" -Force )
Invoke-WebRequest https://path/to/Welcome-ToastNotification.ps1 -Outfile ( New-Item -Path "C:\ProgramData\toasty\Welcome-ToastNotification.ps1" -Force )
Start-Process powershell.exe -WindowStyle Hidden -ArgumentList "-executionpolicy bypass -File $path"
}
else {
    Write-Output "No action required"
}